# WaveIEEbsProject

#### 介绍
现在国内有好多企业还在用美国的EBS 做为企业的ERP管理软件，但现在中国主推的操作系统是DEEPIN /UOS 这个操作系统在国内使用率很高，但大部分都政府单位，如果一个企业要用，并且他的ERP 用的是美国的EBS ，那怎么处理要好呢？本项目的作用，就是帮组这些企业平衡的过度的到DEEPIN /UOS，作者也准备把源码开源出来，让更多人去适配其他的国产系统。在用的适合，如果你觉得有用，请打赏一下，开源也得需要养家

#### 软件架构
软件是基于C++开发，在国产系统DEEPIN进行过适配，


#### 安装教程

1.  在mydeb/WeIE.deb 文件可以直接安装，这个有授权的，
2. 如果你不想用有授权的，可以用源码编译 一下，把授权验证的代码给删除了，再编译打包。


#### 使用说明

1.  在桌面会生成一个WeIE可以打开使用，


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
