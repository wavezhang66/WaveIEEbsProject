
#include <string.h>
#include <assert.h>
#include <iostream>
#include "WeInstallAft.h"
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <fstream>
#include "WeComm/WePublicCom.h"
#include "WeComm/WeAes.h"
using namespace std;
WeInstallAft::WeInstallAft()
{
}

WeInstallAft::~WeInstallAft()
{
}
/*int main()
{
  int result = 0;
  WeInstallAft weInstallAft;
  string path = "/opt/apps/com.we.weie";
  result = weInstallAft.install(path);
  return result;
}*/
int WeInstallAft::install(std::string path)
{
  int result;
  int installResult;
  string commdString;
  WePublicCom wePublicCom;
  WeAes weAes;
  installResult = 0;

  commdString = "rm " + path + "/WeLib/WeFirefox/update-settings.ini";
  result = system(commdString.c_str());
  commdString = "rm " + path + "/WeLib/WeFirefox/updater";
  result = system(commdString.c_str());
  commdString = "rm " + path + "/WeLib/WeFirefox/updater.ini";
  result = system(commdString.c_str());
  commdString = "mv " + path + "/WeLib/WeFirefox/firefox " + path + "/WeLib/WeFirefox/WeIEMain ";
  result = system(commdString.c_str());
  commdString = "\\cp -rf " + path + "/WeLib/WeJre/lib/fonts/fonts.dir " + path + "/WeFont";
  result = system(commdString.c_str());

  cout << "will get userHomePath" << endl;
  string userHomePath = "/home/"+wePublicCom.getUserName(); /* wePublicCom.getCurrentUserHomePath("~");*/
  

  fstream file;
  file.open(userHomePath + "/.mozilla/plugins", ios::in);
  cout << "userHomePath:" << userHomePath << endl;
  if (!file)
  {
    commdString = "sudo mkdir " + userHomePath + "/.mozilla/plugins";

    result = system(commdString.c_str());
    cout << "commdString:" << commdString << ":" << result << endl;
  }
  else
  {
    file.close();
  }
  file.open(userHomePath + "/.mozilla/plugins/libnpjp2.so", ios::in);
  if (!file)
  {
    commdString = "cd " + userHomePath + "/.mozilla/plugins && ln -s " + path + "/WeLib/WeJre/lib/amd64/libnpjp2.so";

    result = system(commdString.c_str());
    cout << "commdString:" << commdString << ":" << result << endl;
  }
  else
  {
    file.close();
    commdString = "rm " + userHomePath + "/.mozilla/plugins/libnpjp2.so";
    result = system(commdString.c_str());
    cout << "commdString:" << commdString << ":" << result << endl;

    commdString = "cd " + userHomePath + "/.mozilla/plugins && ln -s " + path + "/WeLib/WeJre/lib/amd64/libnpjp2.so";
    result = system(commdString.c_str());
    cout << "commdString:" << commdString << ":" << result << endl;
  }

  ofstream ifs;

  if (installResult == 0)
  {
    file.open(userHomePath + "/.mozilla/plugins/WeTestKey.we", ios::in);
    if (!file)
    {
      //用来存放许测试license，也就是试用版的
      char cyear[50];
      char cmonth[50];
      char cday[50];

      time_t now = time(NULL);
      strftime(cyear, 50, "%Y", localtime(&now));
      strftime(cmonth, 50, "%m", localtime(&now));
      strftime(cday, 50, "%d", localtime(&now));
      int year = atoi(cyear);
      int month = atoi(cmonth);
      int day = atoi(cday);
      //算出有效期
      month = month + 4;
      if (month > 12)
      {
        month = month - 12;
        year = year + 1;
      }
      stringstream syear;
      stringstream smon;
      syear << year;
      smon << month;
      string enddate;
      if (smon.str().length() > 1)
      {
        enddate = syear.str() + "-" + smon.str() + "-" + "01";
      }
      else
      {
        enddate = syear.str() + "-" + "0" + smon.str() + "-" + "01";
      }

      ifs.open(userHomePath + "/.mozilla/plugins/WeTestKey.we", ios::out);
      ifs << weAes.EncryptionAES(enddate) << endl;
      ifs << "End date:" + enddate << endl;
      ifs << "有效期止:" + enddate << endl;
      ifs.close();
    }
  }
  //获得CUP序列号，生成CUP序列号的密码文
  if (installResult == 0)
  {
    string cpuid;
    ifs.open(path + "/WeReqKey.we", ios::out);
    if (wePublicCom.get_cpu_id(cpuid))
    {
      ifs << weAes.EncryptionAES("cpuid:" + cpuid) << endl;
    }
    else
    {
      ifs << "Can't get Cpuid" << endl;
    }
    ifs.close();
    //用来存放许license
    ifs.open(path + "/WeKey.we", ios::out);
    ifs << "获得license之后，把当前行替换成license,一年有效期20元，终身版100元 QQ微信同号：284666889，pls replace the line after get license" << endl;
    ifs.close();
  }
  //生产桌面图标
  if (installResult == 0)
  {
    commdString = "\\cp -rf  /usr/share/applications/WeIE.desktop " + userHomePath + "/Desktop";
    result = system(commdString.c_str());
  }

  return installResult;
}
int WeInstallAft::checkconfig(std::string path)
{
  int result;
  int installResult;
  string commdString;
  WePublicCom wePublicCom;
  WeAes weAes;
  installResult = 0;
  cout << "will get userHomePath" << endl;
  string userHomePath = "/home/"+wePublicCom.getUserName(); /* wePublicCom.getCurrentUserHomePath("~");*/
  

  fstream file;
  file.open(userHomePath + "/.mozilla/plugins", ios::in);
  cout << "userHomePath:" << userHomePath << endl;
  if (!file)
  {
    commdString = "mkdir " + userHomePath + "/.mozilla/plugins";

    result = system(commdString.c_str());
    cout << "commdString:" << commdString << ":" << result << endl;
  }
  else
  {
    file.close();
  }
  file.open(userHomePath + "/.mozilla/plugins/libnpjp2.so", ios::in);
  if (!file)
  {
    commdString = "cd " + userHomePath + "/.mozilla/plugins && ln -s " + path + "/WeLib/WeJre/lib/amd64/libnpjp2.so";

    result = system(commdString.c_str());
    cout << "commdString:" << commdString << ":" << result << endl;
  }
 

  ofstream ifs;

  if (installResult == 0)
  {
    file.open(userHomePath + "/.mozilla/plugins/WeTestKey.we", ios::in);
    if (!file)
    {
      //用来存放许测试license，也就是试用版的
      char cyear[50];
      char cmonth[50];
      char cday[50];

      time_t now = time(NULL);
      strftime(cyear, 50, "%Y", localtime(&now));
      strftime(cmonth, 50, "%m", localtime(&now));
      strftime(cday, 50, "%d", localtime(&now));
      int year = atoi(cyear);
      int month = atoi(cmonth);
      int day = atoi(cday);
      //算出有效期
      month = month + 4;
      if (month > 12)
      {
        month = month - 12;
        year = year + 1;
      }
      stringstream syear;
      stringstream smon;
      syear << year;
      smon << month;
      string enddate;
      if (smon.str().length() > 1)
      {
        enddate = syear.str() + "-" + smon.str() + "-" + "01";
      }
      else
      {
        enddate = syear.str() + "-" + "0" + smon.str() + "-" + "01";
      }

      ifs.open(userHomePath + "/.mozilla/plugins/WeTestKey.we", ios::out);
      ifs << weAes.EncryptionAES(enddate) << endl;
      ifs << "End date:" + enddate << endl;
      ifs << "有效期止:" + enddate << endl;
      ifs.close();
    }
  }
  

  return installResult;
}
