/*****************************************************************************
*  Copyright (C) 2020 Wave.Zhang  wavezhang@163.com.                         *
*                                                                            *
*                                                                            *
*  @file     WeGenKey.h                                                       *
*  @brief    生成授权码 、                                                     *
*  Details.                                                                  *
*                                                                            *
*  @author   Wave.Zhang                                                       *
*  @email    wavezhang@163.com                                              *
*  @version  1.0.0.1(版本号)                                                 *
*  @date     wavezhang@163.com                                              *
*  @license                                 *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  2020/07/31 |  1.0.0.1   | Wave.Zhang      | Create file                     *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#include <iostream>
#include <vector>
#include <string>
#include <assert.h>
#include <iostream>
#include <fstream>

#include "WeComm/WePublicCom.h"
#include "WeComm/WeAes.h"
#include "WeComm/WeString.h"
#include "WeIEEbsInstall.h"

using namespace std;
int main(){
  WePublicCom wePublicCom;
  WeAes weAes;
  WeString weString;
  string license; 
  string licenseWord;

  string cupid;
  wePublicCom.get_cpu_id(cupid);
  cout<<"cupid:"<<cupid<<endl;

  cout<<"请输入用户的请求信息："<<endl;
  getline(cin,licenseWord);
  string cpuWord=weAes.DecryptionAES(licenseWord);
  cout<<"license明文:"<<cpuWord<<endl;

  cout<<"将要生成 license，请输入用户的完整请求信息："<<endl;
  getline(cin,licenseWord);
  license=weAes.EncryptionAES(licenseWord);
  cout<<"注册license:"<<license<<endl;
  licenseWord=weAes.DecryptionAES(license);
  cout<<"license明文:"<<licenseWord<<endl;
  return 0;
}
