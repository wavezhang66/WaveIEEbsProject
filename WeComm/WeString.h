/*****************************************************************************
*  Copyright (C) 2019 Wave.Zhang  wavezhang@163.com.                         *
*  WeIe is licensed under Mulan PSL v2.                                 *
* You can use this software according to the terms and conditions of the Mulan PSL v2. *
*   You may obtain a copy of Mulan PSL v2 at:
*            http://license.coscl.org.cn/MulanPSL2 
*   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,*
* INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  *
*   See the Mulan PSL v2 for more details.                                   *
*                                                                            *
*  @file     WeString.h                                                       *
*  @brief    对C++字符串处理功能增加                                                     *
*  Details.                                                                  *
*                                                                            *
*  @author   Wave.Zhang                                                       *
*  @email    wavezhang@163.com                                              *
*  @version  1.0.0.1(版本号)                                                 *
*  @date     wavezhang@163.com                                              *
*  @license                                 *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  2019/07/01 |  1.0.0.1   | Wave.Zhang      | Create file                     *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#include <string>

class WeString {
public:
    WeString();
    ~WeString();
    std::string substring(std::string ch, int pos, int length);
    wchar_t* MBCS2Unicode(wchar_t* buff, const char* str);
    char* Unicode2MBCS(char* buff, const wchar_t* str);
    std::wstring str2wstr(std::string str);
    int wputs(const wchar_t* wstr);
    int wputs(std::wstring wstr);
    std::string wstr2str(std::wstring wstr);
      /*****************************************************************************
 *  Funtion/Procedure   strSub
 *  @author   Wave.Zhang                                                       *
 *  @email    wavezhang@163.com                                              *
 *  @version  1.0.0.1(版本号)                                                 *
 *  @date                                                 *
 *  @license                                              *
 *  paramters:
 *  type        name      in/out        desc
 *  string     srcStrng       in        原字符串 *
 *  string     splitString    in        分隔符 *
 *   int       postion       in         第几个（从1开始） *
 * *----------------------------------------------------------------------------*
 *  Remark         : 以特定字符做为分隔符，并取第几个分隔符，                                              *
 *----------------------------------------------------------------------------*
 *  Change History :                                                          *
 *  <Date>     | <Version> | <Author>       | <Description>                   *
 *----------------------------------------------------------------------------*
 *  2019/07/31 |  1.0.0.1   | Wave.Zhang      | Created                    *
 *----------------------------------------------------------------------------*
 * *****************************************************************************/
    std::string strSub(std::string srcStrng,std::string splitString,int postion);

};
