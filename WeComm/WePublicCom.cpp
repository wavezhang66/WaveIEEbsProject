
#include <string.h>
#include <assert.h>
#include "WePublicCom.h"
#include <stdio.h>

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <pwd.h>
#include <fstream>
using namespace std;
WePublicCom::WePublicCom()
{
}

WePublicCom::~WePublicCom()
{
}
string WePublicCom::getCurrentUserHomePath(std::string path) {
  if (not path.empty() and path[0] == '~'){
    assert(path.size() == 1 or path[1] == '/');  // or other error handling
    char const* home = getenv("HOME");
    if (home or ((home = getenv("USERPROFILE")))) {
      path.replace(0, 1, home);
    }
    else {
      char const *hdrive = getenv("HOMEDRIVE"),
        *hpath = getenv("HOMEPATH");
      assert(hdrive);  // or other error handling
      assert(hpath);
      path.replace(0, 1, std::string(hdrive) + hpath);
    }
  }
  return path;
}
 bool WePublicCom::get_cpu_id_by_asm(std::string & cpu_id)
{
    cpu_id.clear();
 
    unsigned int s1 = 0;
    unsigned int s2 = 0;
    asm volatile
    (
        "movl $0x01, %%eax; \n\t"
        "xorl %%edx, %%edx; \n\t"
        "cpuid; \n\t"
        "movl %%edx, %0; \n\t"
        "movl %%eax, %1; \n\t"
        : "=m"(s1), "=m"(s2)
    );
 
    if (0 == s1 && 0 == s2)
    {
        return(false);
    }
 
    char cpu[32] = { 0 };
    snprintf(cpu, sizeof(cpu), "%08X%08X", htonl(s2), htonl(s1));
    std::string(cpu).swap(cpu_id);
 
    return(true);
}
 
void WePublicCom::parse_cpu_id(const char * file_name, const char * match_words, std::string & cpu_id)
{
    cpu_id.c_str();
 
    std::ifstream ifs(file_name, std::ios::binary);
    if (!ifs.is_open())
    {
        return;
    }
 
    char line[4096] = { 0 };
    while (!ifs.eof())
    {
        ifs.getline(line, sizeof(line));
        if (!ifs.good())
        {
            break;
        }
 
        const char * cpu = strstr(line, match_words);
        if (NULL == cpu)
        {
            continue;
        }
        cpu += strlen(match_words);
 
        while ('\0' != cpu[0])
        {
            if (' ' != cpu[0])
            {
                cpu_id.push_back(cpu[0]);
            }
            ++cpu;
        }
 
        if (!cpu_id.empty())
        {
            break;
        }
    }
 
    ifs.close();
}
 
bool WePublicCom::get_cpu_id_by_system(std::string & cpu_id)
{
    cpu_id.c_str();
 
    const char * dmidecode_result = ".dmidecode_result.txt";
    char command[512] = { 0 };
    snprintf(command, sizeof(command), "dmidecode -t 4 | grep ID > %s", dmidecode_result);
 
    if (0 == system(command))
    {
        parse_cpu_id(dmidecode_result, "ID:", cpu_id);
    }
 
    unlink(dmidecode_result);
 
    return(!cpu_id.empty());
}
 
bool WePublicCom::get_cpu_id(std::string & cpu_id)
{
    if (get_cpu_id_by_asm(cpu_id))
    {
        return(true);
    }
    if (0 == getuid())
    {
        if (get_cpu_id_by_system(cpu_id))
        {
            return(true);
        }
    }
    return(false);
}
 void WePublicCom::test_1()
{
    std::string cpu_id;
    if (get_cpu_id(cpu_id))
    {
        printf("cpu_id: [%s]\n", cpu_id.c_str());
    }
    else
    {
        printf("can not get cpu id\n");
    }
}
 
 void WePublicCom::test_2getcupid_asm()
{
    {
        std::string cpu_id;
        if (get_cpu_id_by_asm(cpu_id))
        {
            printf("cpu_id_by_asm: [%s]\n", cpu_id.c_str());
        }
        else
        {
            printf("can not get cpu id\n");
        }
    }
    {
        std::string cpu_id;
        if (get_cpu_id_by_system(cpu_id))
        {
            printf("cpu_id_by_sys: [%s]\n", cpu_id.c_str());
        }
        else
        {
            printf("can not get cpu id\n");
        }
    }
}

string WePublicCom::getUserName()
{

    uid_t userid;
    struct passwd* pwd;
    userid=getuid();
    pwd=getpwuid(userid);
    return pwd->pw_name;
 
}


