/*****************************************************************************
*  Copyright (C) 2020 Wave.Zhang  wavezhang@163.com.                         *
*  WeIe is licensed under Mulan PSL v2.                                 *
* You can use this software according to the terms and conditions of the Mulan PSL v2. *
*   You may obtain a copy of Mulan PSL v2 at:
*            http://license.coscl.org.cn/MulanPSL2 
*   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,*
* INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  *
*   See the Mulan PSL v2 for more details.                                   *
*                                                                            *
*  @file     WeAe.h                                                       *
*  @brief    AE公用方法 、                                                     *
*  Details.                                                                  *
*                                                                            *
*  @author   Wave.Zhang                                                       *
*  @email    wavezhang@163.com                                              *
*  @version  1.0.0.1(版本号)                                                 *
*  @date                                                 *
*  @license                                 *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  2019/07/31 |  1.0.0.1   | Wave.Zhang      | Create file                    *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#ifndef WeAes_H
#define WeAes_H

#include <string>


class WeAes
{
public: WeAes(); 
 /*****************************************************************************
 *  Funtion/Procedure   EncryptionAES
 *  @author   Wave.Zhang                                                       *
 *  @email    wavezhang@163.com                                              *
 *  @version  1.0.0.1(版本号)                                                 *
 *  @date                                                 *
 *  @license                                              *
 *   type        name      in/out        desc
 *  string&    strSrc       in        明文的地址             *
 * *----------------------------------------------------------------------------*
 *  Remark         : Ae的加密方法                                              *
 *----------------------------------------------------------------------------*
 *  Change History :                                                          *
 *  <Date>     | <Version> | <Author>       | <Description>                   *
 *----------------------------------------------------------------------------*
 *  2019/07/31 |  1.0.0.1   | Wave.Zhang      | Created                    *
 *----------------------------------------------------------------------------*
 * *****************************************************************************/
 std::string EncryptionAES(const std::string& strSrc);
  /*****************************************************************************
 *  Funtion/Procedure   DecryptionAES
 *  @author   Wave.Zhang                                                       *
 *  @email    wavezhang@163.com                                              *
 *  @version  1.0.0.1(版本号)                                                 *
 *  @date                                                 *
 *  @license                                              *
 *  paramters:
 *  type        name      in/out        desc
 *  string&    strSrc       in        密文的地址 *
 * *----------------------------------------------------------------------------*
 *  Remark         : Ae的解密方法                                              *
 *----------------------------------------------------------------------------*
 *  Change History :                                                          *
 *  <Date>     | <Version> | <Author>       | <Description>                   *
 *----------------------------------------------------------------------------*
 *  2019/07/31 |  1.0.0.1   | Wave.Zhang      | Created                    *
 *----------------------------------------------------------------------------*
 * *****************************************************************************/
 std::string DecryptionAES(const std::string& strSrc);

	
};
#endif 
