/*****************************************************************************
*  Copyright (C) 2020 Wave.Zhang  wavezhang@163.com.                         *
*  WeIe is licensed under Mulan PSL v2.                                 *
* You can use this software according to the terms and conditions of the Mulan PSL v2. *
*   You may obtain a copy of Mulan PSL v2 at:
*            http://license.coscl.org.cn/MulanPSL2 
*   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,*
* INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  *
*   See the Mulan PSL v2 for more details.                                   *
*                                                                            *
*  @file     WePublicComm.h                                                       *
*  @brief    项目普通公用方法 、                                                     *
*  Details.                                                                  *
*                                                                            *
*  @author   Wave.Zhang                                                       *
*  @email    wavezhang@163.com                                              *
*  @version  1.0.0.1(版本号)                                                 *
*  @date     wavezhang@163.com                                              *
*  @license                                 *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  2020/07/31 |  1.0.0.1   | Wave.Zhang      | Create file                     *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#include <string>
using namespace std;
class WePublicCom
{
public:
	WePublicCom();
    ~WePublicCom();
public:
	string getCurrentUserHomePath(std::string path);
	void getcpuid (char *id);
	void cpuid(unsigned int veax1);
	void LM(unsigned int var,uint32_t *vx);
	bool get_cpu_id(std::string & cpu_id);
	bool get_cpu_id_by_system(std::string & cpu_id);
	void parse_cpu_id(const char * file_name, const char * match_words, std::string & cpu_id);
	bool get_cpu_id_by_asm(std::string & cpu_id);
	void test_2getcupid_asm();
	void test_1();
	string getUserName();

};