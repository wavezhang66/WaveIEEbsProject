/*****************************************************************************
*  Copyright (C) 2019 Wave.Zhang  wavezhang@163.com.                         *
*  WeIe is licensed under Mulan PSL v2.                                 *
* You can use this software according to the terms and conditions of the Mulan PSL v2. *
*   You may obtain a copy of Mulan PSL v2 at:
*            http://license.coscl.org.cn/MulanPSL2 
*   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,*
* INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  *
*   See the Mulan PSL v2 for more details.                                   *
*                                                                            *
*  @file     Base64.h                                                       *
*  @brief    Base64字符串处理                                            *
*  Details.                                                                  *
*                                                                            *
*  @author   Wave.Zhang                                                       *
*  @email    wavezhang@163.com                                              *
*  @version  1.0.0.1(版本号)                                                 *
*  @date     wavezhang@163.com                                              *
*  @license                                 *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  2019/07/01 |  1.0.0.1   | Wave.Zhang      | Create file                     *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#ifndef BASE_64_H
#define BASE_64_H
#include <string>
std::string base64_encode(unsigned char const*, unsigned int len);
std::string base64_decode(std::string const& s);
#endif
