/*****************************************************************************
*  Copyright (C) 2020 Wave.Zhang  wavezhang@163.com.                         *
*  WeIe is licensed under Mulan PSL v2.                                 *
* You can use this software according to the terms and conditions of the Mulan PSL v2. *
*   You may obtain a copy of Mulan PSL v2 at:
*            http://license.coscl.org.cn/MulanPSL2 
*   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,*
* INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  *
*   See the Mulan PSL v2 for more details.                                   *
*                                                                            *
*  @file     WeInstallAft.h                                                  *
*  @brief    程序自动安装之后运行的程序 、                                        *
*  Details.                                                                  *
*                                                                            *
*  @author   Wave.Zhang                                                      *
*  @email    wavezhang@163.com                                              *
*  @version  1.0.0.1(版本号)                                                 *
*  @date     wavezhang@163.com                                              *
*  @license                                 *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  2020/07/31 |  1.0.0.1   | Wave.Zhang      | Create file                     *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#include <string>
using namespace std;
class WeInstallAft
{
public:
	WeInstallAft();
    ~WeInstallAft();
public:
	int install(std::string path);
	int checkconfig(std::string path);
};