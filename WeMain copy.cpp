/*****************************************************************************
*  Copyright (C) 2020 Wave.Zhang  wavezhang@163.com.                         *
*                                                                            *
*                                                                            *
*  @file     WaveIEEbsInstallMain.h                                                       *
*  @brief    项目安装开始类 、                                                     *
*  Details.                                                                  *
*                                                                            *
*  @author   Wave.Zhang                                                       *
*  @email    wavezhang@163.com                                              *
*  @version  1.0.0.1(版本号)                                                 *
*  @date     wavezhang@163.com                                              *
*  @license                                 *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  2020/07/31 |  1.0.0.1   | Wave.Zhang      | Create file                     *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#include <iostream>
#include <vector>
#include <string>
#include <assert.h>
#include <iostream>
#include <fstream>

#include "WeComm/WePublicCom.h"
#include "WeComm/WeAes.h"
#include "WeComm/WeString.h"
#include "WeIEEbsInstall.h"

using namespace std;

int main()
{
  WePublicCom wePublicCom;
  WeAes weAes;
  WeString weString;
  string commdString;
  string userInstallPath ="/opt/apps";
  /* wePublicCom.getCurrentUserHomePath("~")*/;
  string userHomePath =wePublicCom.getCurrentUserHomePath("~");
  string path;
  path = userInstallPath + "/WaveIEEbs";
  int result;

  /*commdString="chmod 644 "+path+"/WeLib/jre1.6.0_45/lib/fonts/fallback/simhei.ttf";
  result=system(commdString.c_str());
  commdString="chmod 644 "+path+"/WeLib/jre1.6.0_45/lib/fonts/fallback/simsun.ttc";
  result=system(commdString.c_str());*/

  string filename = path + "/WeKey.we";
  string testFilename = userHomePath + "/.mozilla/plugins/WeTestKey.we";
  fstream fin;
  fstream testFin;
  fin.open(filename.c_str(), ios::in);

  string lineString;
  string testLineString;
  int lineNum = 0;
  int testLineNum = 0;
  string licenseWord;
  string licenseMessage;
  int licenseResult = -1;
  string licenseUser;
  string licenseDate;
  string licenseEmail;
  string licenseCupid;
  string currentCupid;
  string versionString="1.0.0.1";

  while (getline(fin, lineString))
  {
    lineNum = lineNum + 1;
    if (lineNum == 1)
    {

      licenseWord = weAes.DecryptionAES(lineString);

      if (licenseWord.empty())
      {
        testFin.open(testFilename.c_str(), ios::in);
        while (getline(testFin, testLineString))
        {
          testLineNum = testLineNum + 1;
          if (testLineNum == 1)
          {
            licenseWord = weAes.DecryptionAES(testLineString);
            if (licenseWord.empty())
            {
              licenseResult = -1;
              licenseMessage = "不能获得正常的授权信息，请购买license：<br>一年有效期20元，终身版100元 <br>微信号：wavezhang66";
            }
            else
            {
              char cyear[50];
              char cmonth[50];
              char cday[50];
              time_t now = time(NULL);
              strftime(cyear, 50, "%Y", localtime(&now));
              strftime(cmonth, 50, "%m", localtime(&now));
              strftime(cday, 50, "%d", localtime(&now));
              string syear = cyear;
              string smonth = cmonth;
              string sday = cday;

              string cuurentDate = syear +"-"+ smonth +"-"+ sday;
              if (licenseWord.compare(cuurentDate) < 0)
              {
                licenseResult = -1;
                licenseMessage = "当前授权信息已过期有效期止：" + licenseWord + ";<br> this license is test version ,the end date:" + licenseWord + ";<br>请重新申请，请购买license：一年有效期20元，终身版100元 ";
              }
              else
              {
                licenseMessage = "你当前的license是试用版，有效期止：" + licenseWord + ";<br>如需要购买正式license，价格与联系方式如下：<br> 一年有效期20元，终身版100元 <br> this license is test version ,the end date:" + licenseWord;
                licenseResult = 0;
              }
            }
          }
        }
        testFin.close();
      }
      else
      {
        // "user:wavezhang&date:20201201&email:wavezhang@163.com&cupid:
        licenseUser = licenseWord.substr(5, licenseWord.find("&date", 0) - 5);
        licenseDate = licenseWord.substr(licenseWord.find("&date:", 0) + 6, licenseWord.find("&email", 0) - (licenseWord.find("&date:", 0) + 6));
        licenseEmail = licenseWord.substr(licenseWord.find("&email:", 0) + 7, licenseWord.find("&cupid", 0) - (licenseWord.find("&email:", 0) + 7));
        licenseCupid = licenseWord.substr(licenseWord.find("&cupid:", 0) + 7, licenseWord.length() - (licenseWord.find("&cupid:", 0) + 7));
        wePublicCom.get_cpu_id(currentCupid);
        if (currentCupid.compare(licenseCupid) == 0)
        {
          char cyear[50];
          char cmonth[50];
          char cday[50];
          time_t now = time(NULL);
          strftime(cyear, 50, "%Y", localtime(&now));
          strftime(cmonth, 50, "%m", localtime(&now));
          strftime(cday, 50, "%d", localtime(&now));
          string syear = cyear;
          string smonth = cmonth;
          string sday = cday;

          string cuurentDate = syear +"-"+ smonth +"-"+ sday;

          if (licenseDate.compare(cuurentDate) < 0)
          {
            licenseResult = -1;
            licenseMessage = "有效期止:"+licenseDate+" <br> 当前授权信息已过期，请重新申请<br>，请购买license：一年有效期20元，终身版100元 ";
            
          }
          else
          {
            licenseMessage="授权信息如下：<br>有效期止:"+licenseDate+"<br>";
            licenseMessage=licenseMessage+"授权用户:"+licenseUser+"<br>";
            
            licenseResult = 0;
          }
        }
        else
        {
          licenseMessage = "当前授权信息是非法的，授权信息与CPU进行了绑定";
        }
      }
    }
  }
  fin.close();
  filename = path + "/Welcome.html";
  fin.open(filename.c_str(), ios::in);
  if (fin)
  {
    commdString = "rm " + path + "/Welcome.html";
    result = system(commdString.c_str());
    fin.close();
  }

  ofstream ifs;
  ifs.open(path + "/Welcome.html", ios::out);
  ifs << "<html>" << endl;
  ifs << "<header>" << endl;
  ifs << "<title>授权信息</title>" << endl;
  ifs << "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">" << endl;
  ifs << "</header>" << endl;
  ifs << "<body>" << endl;
  ifs << licenseMessage << endl;
  ifs << "<br>无论你使用的是正式版，还是试用版，只要遇到问题都可以联系我们" << endl;
  ifs << "<br>唯一科技欢迎您的使用，<br>微信号：wavezhang66" << endl;
  ifs << "<br>版本号："+versionString << endl;
  ifs << "</body></html>" << endl;
  ifs.close();

  commdString = "\\cp -rf ./WeFont/fonts.dir " + path + "/WeLib/jre1.6.0_45/lib/fonts";
  result = system(commdString.c_str());

  commdString = "rm -rf " + path + "/WeLib/jre1.6.0_45/lib/fonts/fallback";
  result = system(commdString.c_str());
  if (licenseResult == 0)
  {
    //授权信息是正常的或者是试用期能的，都可以正常使用。

    commdString = "mkdir " + path + "/WeLib/jre1.6.0_45/lib/fonts/fallback";
    result = system(commdString.c_str());

    commdString = "\\cp -rf " + path + "/WeFont/simhei.ttf " + path + "/WeLib/jre1.6.0_45/lib/fonts/fallback";
    result = system(commdString.c_str());
    commdString = "\\cp -rf " + path + "/WeFont/simsunb.ttf " + path + "/WeLib/jre1.6.0_45/lib/fonts/fallback";
    result = system(commdString.c_str());
    commdString = "\\cp -rf " + path + "/WeFont/simsun.ttc " + path + "/WeLib/jre1.6.0_45/lib/fonts/fallback";
    result = system(commdString.c_str());
    commdString = "cd " + path + "/WeLib/jre1.6.0_45/lib/fonts/fallback && mkfontscale && mkfontdir && cat fonts.scale >> ../fonts.dir && fc-cache";
    result = system(commdString.c_str());
  }

  commdString = "cd " + path + "/WeLib/firefox/ && ./WeIEMain " + path + "/Welcome.html";
  result = system(commdString.c_str());

  /*
  string license=weAes.EncryptionAES("wavezhang&20201201&wavezhang@163.com");
  cout<<"license:"<<license<<endl;
   licenseWord=weAes.DecryptionAES(license);
  cout<<"licenseWord:"<<licenseWord<<endl;
  
  string cupid;
  wePublicCom.get_cpu_id(cupid);
  cout<<"cupid:"<<cupid<<endl;
  license=weAes.EncryptionAES("user:wavezhang&date:2020-12-01&email:wavezhang@163.com&cupid:"+cupid);
  cout<<"license:"<<license<<endl;
  licenseWord=weAes.DecryptionAES(license);
  cout<<"licenseWord:"<<licenseWord<<endl;*/
  return 0;
}
