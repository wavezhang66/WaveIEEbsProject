
#include <string.h>
#include <assert.h>
#include <iostream>
#include "WeIEEbsInstall.h"
#include <stdlib.h>
#include <stdio.h>
#include<sstream>
#include <fstream>
#include "WeComm/WePublicCom.h"
#include "WeComm/WeAes.h"
using namespace std;
WeIEEbsInstall::WeIEEbsInstall()
{
}

WeIEEbsInstall::~WeIEEbsInstall()
{
}
int WeIEEbsInstall::install(std::string path) {
  int result ;
  int installResult;
  string commdString;
  WePublicCom wePublicCom;
  WeAes weAes;
  installResult=0;
  // START 拷贝 库文件
  commdString="mkdir "+path+"/WeLib";
  result=system(commdString.c_str());
  cout << commdString << "   result：" <<result<< endl;
  commdString="\\cp -rf ./WeLib/*.* "+path+"/WeLib";
  result=system(commdString.c_str());
  cout << commdString << "   result：" <<result<< endl;

   commdString="mkdir "+path+"/WeIcon";
  result=system(commdString.c_str());
  cout << commdString << "   result：" <<result<< endl;
    commdString="\\cp -rf ./WeIcon/*.* "+path+"/WeIcon";
  result=system(commdString.c_str());
  cout << commdString << "   result：" <<result<< endl;

  //END 拷贝库文件

  //START拷贝中文字体
  commdString="mkdir "+path+"/WeFont";
  result=system(commdString.c_str());
  cout << commdString << "   result：" <<result<< endl;

  commdString="\\cp -rf ./WeFont/*.* "+path+"/WeFont";
  result=system(commdString.c_str());
  cout << commdString << "   result：" <<result<< endl;

  commdString="\\cp -rf ./WeIE "+path+"/";
  result=system(commdString.c_str());
  cout << commdString << "   result：" <<result<< endl;
  //END 拷贝字体
  // 给拷贝过来的文件加权限 chmod,并释放文件
  commdString="chmod 755 "+path+"/WeLib/WeJre.bin";
  result=system(commdString.c_str());
  cout << commdString << "   result：" <<result<< endl;
  commdString="cd "+path+"/WeLib&&./WeJre.bin";
  result=system(commdString.c_str());
  cout << commdString << "   result：" <<result<< endl;

  commdString="tar -jxvf "+path+"/WeLib/Wefirefox.tar.bz2 -C "+ path+"/WeLib";
  result=system(commdString.c_str());
  cout << commdString << "   result：" <<result<< endl;

  commdString="rm "+path+"/WeLib/firefox/update-settings.ini";
  result=system(commdString.c_str());
  commdString="rm "+path+"/WeLib/firefox/updater";
  result=system(commdString.c_str());  
  commdString="rm "+path+"/WeLib/firefox/updater.ini";
  result=system(commdString.c_str());
  commdString="mv "+path+"/WeLib/firefox/firefox "+path+"/WeLib/firefox/WeIEMain ";
  result=system(commdString.c_str());
  commdString="\\cp -rf "+path+"/WeLib/jre1.6.0_45/lib/fonts/fonts.dir "+path+"/WeFont";
  result=system(commdString.c_str());  
  commdString="\\cp -rf ./WeIcon/*.* "+path+"/WeLib/firefox/browser/chrome/icons/default";
  result=system(commdString.c_str());
  commdString="\\cp -rf ./WeIcon/mozicon128.png "+path+"/WeLib/firefox/browser/icons";
  result=system(commdString.c_str());
  

    string userHomePath =wePublicCom.getCurrentUserHomePath("~");
 
    fstream file;
    file.open(userHomePath+"/.mozilla/plugins", ios::in);
   
    if (!file)
    {
      commdString="mkdir "+userHomePath+"/.mozilla/plugins";
      result=system(commdString.c_str());
    }else{
      file.close();
      file.open(userHomePath+"/.mozilla/plugins/libnpjp2.so", ios::in);
      if (!file)
    {
      commdString="cd "+userHomePath+"/.mozilla/plugins && ln -s "+path+"/WeLib/jre1.6.0_45/lib/amd64/libnpjp2.so";
      result=system(commdString.c_str());
    }else{
      file.close();
      commdString="rm "+userHomePath+"/.mozilla/plugins/libnpjp2.so";
      result=system(commdString.c_str());
      commdString="cd "+userHomePath+"/.mozilla/plugins && ln -s "+path+"/WeLib/jre1.6.0_45/lib/amd64/libnpjp2.so";
      result=system(commdString.c_str());

    }

    }

    
    ofstream ifs;
    
    if (installResult==0){
      file.open(userHomePath+"/.mozilla/plugins/WeTestKey.we", ios::in);
      if (!file)
    {
      //用来存放许测试license，也就是试用版的
      char cyear[50];
      char cmonth[50];
      char cday[50];

      time_t now = time(NULL);
      strftime(cyear, 50, "%Y", localtime(&now));
      strftime(cmonth, 50, "%m", localtime(&now));
      strftime(cday, 50, "%d", localtime(&now));
      int year=atoi(cyear);
      int month=atoi(cmonth);
      int day=atoi(cday);
      //算出有效期
      month=month+4;
      if (month>12){
        month=month-12;
        year=year+1;
      }
      stringstream syear;
      stringstream smon;
      syear<<year;
      smon<<month;
      string enddate;
      if (smon.str().length()>1){
        enddate=syear.str()+"-"+smon.str()+"-"+"01";
      }else{
        enddate=syear.str()+"-"+"0"+smon.str()+"-"+"01";
      }     

       ifs.open(userHomePath+"/.mozilla/plugins/WeTestKey.we", ios::out);        
       ifs <<weAes.EncryptionAES(enddate)<< endl;
       ifs <<"End date:"+enddate<< endl;
       ifs <<"有效期止:"+enddate<< endl;
       ifs.close();
    }
    }
    //获得CUP序列号，生成CUP序列号的密码文
    if (installResult==0){
      string cpuid;
      ifs.open(path+"/WeReqKey.we", ios::out);
      if(wePublicCom.get_cpu_id(cpuid)){
         ifs << weAes.EncryptionAES("cpuid:"+cpuid) << endl;
      }else{
         ifs << "Can't get Cpuid" << endl;
      }
      ifs.close();
       //用来存放许license
       ifs.open(path+"/WeKey.we", ios::out); 
       ifs << "获得license之后，把当前行替换成license,一年有效期20元，终身版100元 微信号：wavezhang66，pls replace the line after get license" << endl;     
       ifs.close();
    }
    //生产桌面图标
   if (installResult==0){
     commdString="\\cp -rf WeIE.desktop "+path;
     result=system(commdString.c_str());
     
      ifs.open(path+"/WeIE.desktop", ios::out);
        ifs << "[Desktop Entry]" << endl;
        ifs << "Categories=GTK;Network;message; //可写可不写" << endl;
        ifs << "Comment=\"Deepin WeIE\"  //提示性信息 ，可写可不写" << endl;
        ifs << "Encoding=UTF-8" << endl;
        ifs << "Exec="+path+"/WeIE %U" << endl;
        ifs << "Icon="+path+"/WeIcon/WeIE.png" << endl;
        ifs << "Info=\"WeIE\"" << endl;
        ifs << "Name=WeIE" << endl;
        ifs << "StartupNotify=true" << endl;
        ifs << "Terminal=false" << endl;
        ifs << "Type=Application" << endl;
        ifs << "X-Deepin-Vendor=user-custom" << endl;

      ifs.close();
      commdString="\\cp -rf  "+path+"/WeIE.desktop "+userHomePath+"/Desktop";
      result=system(commdString.c_str());

   }

   return installResult;
}
