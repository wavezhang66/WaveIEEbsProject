/*****************************************************************************
*  Copyright (C) 2020 Wave.Zhang  wavezhang@163.com.                         *
*                                                                            *
*                                                                            *
*  @file     WaveIEEbsInstallMain.h                                                       *
*  @brief    项目安装开始类 、                                                     *
*  Details.                                                                  *
*                                                                            *
*  @author   Wave.Zhang                                                       *
*  @email    wavezhang@163.com                                              *
*  @version  1.0.0.1(版本号)                                                 *
*  @date     wavezhang@163.com                                              *
*  @license                                 *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  2020/07/31 |  1.0.0.1   | Wave.Zhang      | Create file                     *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#include <iostream>
#include <vector>
#include <string>
#include <assert.h>
#include <iostream>
#include <fstream>

#include "WeComm/WePublicCom.h"
#include "WeIEEbsInstall.h"
using namespace std;

int main()
{
    string userHomePath;
    string userInstallPath ="/opt/apps";
    WePublicCom wePublicCom;
    WeIEEbsInstall weIEEbsInstall;
     /* vector<string> msg{"Hello", "C++", "World", "from", "VS Code", "and the C++ extension!"};
    for (const string &word : msg)
    {
        cout << word << " ";
    }*/
    userHomePath = wePublicCom.getCurrentUserHomePath("~");
    userInstallPath=userInstallPath+"/WaveIEEbs";
    string commdString;
    fstream file;
    file.open(userInstallPath, ios::in);
    int result;
    if (!file)
    {
        commdString="mkdir "+ userInstallPath;     
        cout << userInstallPath << " is not existed !" << endl;
        cout << userInstallPath << " will create!" << endl;
        result=system(commdString.c_str());
        cout << userInstallPath << " result is "<< result << endl;
        if (result==0){
            cout << userInstallPath << "  create successfully" << endl;
            result = weIEEbsInstall.install(userInstallPath);
            
            cout << userInstallPath << "  install result" <<result<< endl;
            
        }else{
            cout << userInstallPath << "  create error ,pls check Permission of the path" << endl;
        }
      
    }
    else
    {
         file.close();
        commdString="rm -rf "+ userInstallPath;
        cout << userInstallPath << "is existed !" << endl;
        cout << userInstallPath << " will delete!" << endl;
        result=system(commdString.c_str());
        cout << userInstallPath << " result is "<< result << endl;
        if (result==0){
            cout << userInstallPath << "  delete successfully" << endl;
            commdString="mkdir "+ userInstallPath;  
        cout << userInstallPath << " will re-create!" << endl;
        result=system(commdString.c_str());
        cout << userInstallPath << " result is "<< result << endl;
        if (result==0){
            cout << userInstallPath << "  create successfully" << endl;
             result = weIEEbsInstall.install(userInstallPath);
            cout << userInstallPath << "  install result" <<result<< endl;
            

        }else{
            cout << userInstallPath << "  create error ,pls check Permission of the path" << endl;
        }
      

        }else{
            cout << userInstallPath << "  delete error ,pls check Permission of the path" << endl;
        }

        
    }

    cout << "current user install path:" << userInstallPath;

    cout << endl;
}
